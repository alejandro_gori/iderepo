﻿Public Class frmAyuda
    Const n As String = vbNewLine
    Dim nodo As String

    Sub infoNodo()
        txtTitulo.Text = nodo.ToUpper()
        Select Case nodo.ToUpper.TrimEnd
            Case "ARCHIVO"
                txtInfo.Text = n + n + "Desplegando el nodo Archivo encontrarás información de:" + n + n + _
                                "Nuevo" + n + _
                                "Abrir" + n + _
                                "Guardar" + n + _
                                "Guardar Como"

            Case "EDITAR"
                txtInfo.Text = n + n + "Desplegando el nodo Editar encontrarás información de:" + n + n + _
                               "Copiar" + n + _
                               "Cortar" + n + _
                               "Pegar" + n + _
                               "Buscar"
            Case " AYUDA"
                txtInfo.Text = n + n + "Desplegando el nodo Editar encontrarás información de:" + n + n + _
                               "Ayuda" + n + _
                               "Acerca de"
            Case "COMPILAR"
                txtInfo.Text = n + n + "Funcion no disponible aún..."
            Case "NUEVO"
                txtInfo.Text = n + n + "Abre un nuevo espacio en las pestañas para editar un documento." + n + n + _
                               "No se pide ruta para abrir el nuevo archivo."
            Case "ABRIR"
                txtInfo.Text = n + n + "Abre un archivo especificado por el usuario, se puede abrir a partir de un documento en blanco, reemplazar un documento abierto " + _
                    "(guarda el documento actual, lo cierra y abre uno nuevo en la misma pestaña) o abrir el documento en otra pestaña."
            Case "GUARDAR"
                txtInfo.Text = n + n + "Guarda el documento actual en el que se encuentra el usuario."
            Case "GUARDAR COMO"
                txtInfo.Text = n + n + "Guarda el documento actual en el que se encuentra el usuario con un nuevo nombre."
            Case "CORTAR"
                txtInfo.Text = n + n + "Corta el texto seleccionado en el texto actual."
            Case "COPIAR"
                txtInfo.Text = n + n + "Copia el texto seleccionado en el texto actual."
            Case "PEGAR"
                txtInfo.Text = n + n + "Pega el texto seleccionado en el texto actual."
            Case "BUSCAR"
                txtInfo.Text = n + n + "Funcion no disponible aún..."
            Case "AYUDA"
                txtInfo.Text = n + n + "Muestra ayuda general sobre el programa."
            Case "ACERCA DE"
                txtInfo.Text = n + n + "Muestra la informacion del programa."
        End Select
    End Sub

    Private Sub tView_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles tView.AfterSelect
        nodo = e.Node.Text
        infoNodo()
    End Sub
End Class