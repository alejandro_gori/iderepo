﻿Option Explicit On
Imports System.IO
Imports System.Text
Public Class Form1
    Public Sub New()
        Try
            InitializeComponent()
            mainStrip.Renderer = New MyRenderer()
        Catch ex As Exception
        End Try
    End Sub

    Dim lstPages As New List(Of TabPage)
    Dim lstRich As New List(Of RichTextBox)
    Dim lstPath As New List(Of String)
    Dim token As New Dictionary(Of String, Color)
    Private token_dictionary As Dictionary(Of String, Color)


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lstPages.Add(Page1)
        lstPages.Add(PageAdd)
        lstPath.Add(Nothing)
        lstRich.Add(rtxtMain)
    End Sub
    Private Sub Form1_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed

    End Sub

    ''
    'Funciones para abrir y cerrar una pestaña
    ''

    Sub nuevaPestaña(Optional ByVal pth As String = Nothing)
        Dim pag As New TabPage("NewPage(" & tabMain.TabPages.Count & ")")
        Dim rich As New RichTextBox()
        rich.Dock = DockStyle.Fill
        lstRich.Add(rich)
        pag.ImageIndex = 0
        pag.Controls.Add(rich)
        AddHandler rich.TextChanged, AddressOf rtxtMain_TextChanged
        AddHandler rich.KeyUp, AddressOf rtxtMain_KeyUp
        lstPages.Insert(lstPages.Count - 1, pag)
        tabMain.TabPages.Insert(tabMain.TabCount - 1, pag)
        tabMain.SelectedIndex = tabMain.TabCount - 2
        lstPath.Add(pth)
    End Sub
    Private Sub tabMain_MouseMove(sender As Object, e As MouseEventArgs) Handles tabMain.MouseMove
        For k = 0 To tabMain.TabPages.Count - 1
            Dim r As Rectangle = tabMain.GetTabRect(k)
            r.Width = 20
            r.Height = 20
            If r.Contains(e.Location) Then
                If lstPages.Item(k).ImageIndex = 0 Then
                    lstPages.Item(k).ImageIndex = 1
                ElseIf lstPages.Item(k).ImageIndex = 2 Then
                    lstPages.Item(k).ImageIndex = 3
                End If
            Else
                If lstPages.Item(k).ImageIndex = 1 Then
                    lstPages.Item(k).ImageIndex = 0
                ElseIf lstPages.Item(k).ImageIndex = 3 Then
                    lstPages.Item(k).ImageIndex = 2
                End If
            End If
        Next
    End Sub
    Private Sub tabMain_Click(sender As Object, e As EventArgs) Handles tabMain.Click
        If tabMain.SelectedIndex = tabMain.TabCount - 1 And lstPages.Item(tabMain.TabCount - 1).ImageIndex = 3 Then
            nuevaPestaña()
        ElseIf tabMain.SelectedIndex <> tabMain.TabCount - 1 And lstPages.Item(tabMain.SelectedIndex).ImageIndex = 1 Then
            lstPages.RemoveAt(tabMain.SelectedIndex)
            lstRich.RemoveAt(tabMain.SelectedIndex)
            lstPath.RemoveAt(tabMain.SelectedIndex)
            tabMain.TabPages(tabMain.SelectedIndex).Dispose()
        End If
    End Sub

    ''
    'Funciones para archivos de texto
    ''
    'leer
    Function leeArchivo(ByVal rtxt As RichTextBox, pth As String) As Boolean
        rtxt.Text = ""
        Try
            Dim sr As New StreamReader(pth, Encoding.UTF7)
            While Not sr.EndOfStream
                rtxt.Text += sr.ReadLine + vbNewLine
            End While
            sr.Close()
            sr.Dispose()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    'guardar
    Function guardaArchivo(ByVal rtxt As RichTextBox, pth As String) As Boolean
        If pth = Nothing Then
            If sArchivo.ShowDialog = Windows.Forms.DialogResult.OK Then
                pth = sArchivo.FileName
                lstPath.Insert(tabMain.SelectedIndex, pth)
                lstPath.RemoveAt(tabMain.SelectedIndex + 1)
            Else
                Return False
            End If
        End If
        Dim fs As New FileStream(pth, FileMode.Create)
        Dim buffer() As Byte = UTF7Encoding.UTF7.GetBytes(rtxt.Text)
        fs.Write(buffer, 0, buffer.Length)
        fs.Close()
        Return True
    End Function


    ''
    'funciones para el HightLighting
    ''
    Private Sub rtxtMain_TextChanged(sender As Object, e As EventArgs) Handles rtxtMain.TextChanged
        Dim cPos As Integer = sender.SelectionStart
        If cPos > 2 Then
            If sender.Find("dim", cPos - 3, cPos, RichTextBoxFinds.WholeWord) > 2 Then
                sender.SelectionColor = Color.Blue
            End If
            sender.DeselectAll()
            sender.SelectionStart = cPos
            sender.SelectionColor = Color.Black
        End If
    End Sub

    ''
    'funciones para los toolstrips
    ''

    'nuevo
    Private Sub NuevoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevoToolStripMenuItem.Click
        nuevaPestaña()
    End Sub
    'abrir
    Private Sub AbrirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AbrirToolStripMenuItem.Click
        If oArchivo.ShowDialog = Windows.Forms.DialogResult.OK Then
            If tabMain.TabCount = 1 Then
                nuevaPestaña(oArchivo.FileName)
                leeArchivo(lstRich.Item(tabMain.SelectedIndex), lstPath.Item(tabMain.SelectedIndex))
                lstPages.Item(tabMain.SelectedIndex).Text = Path.GetFileName(lstPath.Item(tabMain.SelectedIndex))
            ElseIf lstRich(tabMain.SelectedIndex).Text.Trim.Length = 0 Then
                If lstPath.Item(tabMain.SelectedIndex) = Nothing Then
                    lstPath.Insert(tabMain.SelectedIndex, oArchivo.FileName)
                    lstPath.RemoveAt(tabMain.SelectedIndex + 1)
                    leeArchivo(lstRich.Item(tabMain.SelectedIndex), lstPath.Item(tabMain.SelectedIndex))
                    lstPages.Item(tabMain.SelectedIndex).Text = Path.GetFileName(lstPath.Item(tabMain.SelectedIndex))
                End If
            Else
                Dim dres As DialogResult = MessageBox.Show("Tiene abierto un archivo en esta pestaña" + vbNewLine + "¿desea abrirlo en una nueva ventana?", "AVISO", _
                                  MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                If dres = Windows.Forms.DialogResult.Yes Then
                    nuevaPestaña(oArchivo.FileName)
                    leeArchivo(lstRich.Item(tabMain.SelectedIndex), lstPath.Item(tabMain.SelectedIndex))
                    lstPages.Item(tabMain.SelectedIndex).Text = Path.GetFileName(lstPath.Item(tabMain.SelectedIndex))
                ElseIf dres = Windows.Forms.DialogResult.No Then
                    guardaArchivo(lstRich.Item(tabMain.SelectedIndex), lstPath.Item(tabMain.SelectedIndex))
                    lstPath.Insert(tabMain.SelectedIndex, oArchivo.FileName)
                    lstPath.RemoveAt(tabMain.SelectedIndex + 1)
                    leeArchivo(lstRich.Item(tabMain.SelectedIndex), lstPath.Item(tabMain.SelectedIndex))
                    lstPages.Item(tabMain.SelectedIndex).Text = Path.GetFileName(lstPath.Item(tabMain.SelectedIndex))
                End If
            End If
            lstRich.Item(tabMain.SelectedIndex).SelectionStart = lstRich.Item(tabMain.SelectedIndex).Text.Length
        End If
    End Sub
    'guardar
    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarToolStripMenuItem.Click
        If guardaArchivo(lstRich.Item(tabMain.SelectedIndex), lstPath.Item(tabMain.SelectedIndex)) Then
            lbl1.Text = "Guardado archivo en: " & lstPath.Item(tabMain.SelectedIndex)
            lstPages.Item(tabMain.SelectedIndex).Text = Path.GetFileName(lstPath.Item(tabMain.SelectedIndex))
        Else
            lbl1.Text = "No se ha podido guardar el archivo porque no se le ha dado una ruta"
        End If
    End Sub
    'guardar como...
    Private Sub GuardarComoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarComoToolStripMenuItem.Click
        If guardaArchivo(lstRich.Item(tabMain.SelectedIndex), Nothing) Then
            lbl1.Text = "Guardado archivo en: " & lstPath.Item(tabMain.SelectedIndex)
            lstPages.Item(tabMain.SelectedIndex).Text = Path.GetFileName(lstPath.Item(tabMain.SelectedIndex))
        Else
            lbl1.Text = "No se ha podido guardar el archivo porque no se le ha dado una ruta"
        End If
    End Sub

    'copiar
    Private Sub CopiarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopiarToolStripMenuItem.Click
        lstRich.Item(tabMain.SelectedIndex).Copy()
    End Sub
    'cortar
    Private Sub CortarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CortarToolStripMenuItem.Click
        lstRich.Item(tabMain.SelectedIndex).Cut()
    End Sub
    'pegar
    Private Sub PegarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PegarToolStripMenuItem.Click
        lstRich.Item(tabMain.SelectedIndex).Paste()
    End Sub
    'abre un nuevo documento
    Private Sub btnAbrir_Click(sender As Object, e As EventArgs) Handles btnAbrir.Click
        nuevaPestaña()
        AbrirToolStripMenuItem.PerformClick()
    End Sub
    'solo sirve para dar esperanza....
    Private Sub btnAct_Click(sender As Object, e As EventArgs) Handles btnAct.Click
        MsgBox("Proximamente abra una actualización.")
    End Sub
    Private Sub ConsolaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsolaToolStripMenuItem.Click
        If File.Exists(".\Consola.exe") Then
            Process.Start(".\Consola.exe")
        End If
    End Sub
    Private Sub rtxtMain_KeyUp(sender As Object, e As KeyEventArgs) Handles rtxtMain.KeyUp
        txtLineaActual.Text = sender.GetLineFromCharIndex(sender.GetFirstCharIndexOfCurrentLine) + 1
        txtCuentaLinea.Text = (lstRich.Item(tabMain.SelectedIndex).GetLineFromCharIndex(lstRich.Item(tabMain.SelectedIndex).Text.Count) + 1).ToString()
    End Sub
    Private Sub AyudaToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles AyudaToolStripMenuItem1.Click
        frmAyuda.Show()
    End Sub
    Private Sub AcercaDeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AcercaDeToolStripMenuItem.Click
        AcercaDe.ShowDialog()
    End Sub
End Class

Public Class MyRenderer
    Inherits ToolStripProfessionalRenderer
    Protected Overloads Overrides Sub OnRenderMenuItemBackground(ByVal e As ToolStripItemRenderEventArgs)
        Try
            Dim rc As New Rectangle(Point.Empty, e.Item.Size)
            Dim c As Color = IIf(e.Item.Selected, Color.Gray, System.Drawing.SystemColors.InactiveCaptionText)
            Using brush As New SolidBrush(c)
                e.Graphics.FillRectangle(brush, rc)
            End Using
        Catch ex As Exception
        End Try
    End Sub
End Class