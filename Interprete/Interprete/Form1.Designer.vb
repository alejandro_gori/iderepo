﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.mainStrip = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AbrirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GuardarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GuardarComoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsolaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopiarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CortarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PegarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BuscarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AyudaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AyudaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AcercaDeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CompilarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tabMain = New System.Windows.Forms.TabControl()
        Me.Page1 = New System.Windows.Forms.TabPage()
        Me.rtxtMain = New System.Windows.Forms.RichTextBox()
        Me.PageAdd = New System.Windows.Forms.TabPage()
        Me.btnAct = New System.Windows.Forms.Button()
        Me.btnAbrir = New System.Windows.Forms.Button()
        Me.imgList = New System.Windows.Forms.ImageList(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.lblStat = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.txtCuentaPalabras = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lbl = New System.Windows.Forms.ToolStripStatusLabel()
        Me.txtLineaActual = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel4 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.txtCuentaLinea = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel5 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lbl1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.oArchivo = New System.Windows.Forms.OpenFileDialog()
        Me.sArchivo = New System.Windows.Forms.SaveFileDialog()
        Me.mainStrip.SuspendLayout()
        Me.tabMain.SuspendLayout()
        Me.Page1.SuspendLayout()
        Me.PageAdd.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'mainStrip
        '
        Me.mainStrip.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.mainStrip.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.mainStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.EditarToolStripMenuItem, Me.AyudaToolStripMenuItem, Me.CompilarToolStripMenuItem})
        Me.mainStrip.Location = New System.Drawing.Point(0, 0)
        Me.mainStrip.Name = "mainStrip"
        Me.mainStrip.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.mainStrip.Size = New System.Drawing.Size(637, 24)
        Me.mainStrip.TabIndex = 5
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevoToolStripMenuItem, Me.AbrirToolStripMenuItem, Me.GuardarToolStripMenuItem, Me.GuardarComoToolStripMenuItem, Me.ConsolaToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ArchivoToolStripMenuItem.Text = "Archivo"
        '
        'NuevoToolStripMenuItem
        '
        Me.NuevoToolStripMenuItem.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.NuevoToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.NuevoToolStripMenuItem.Name = "NuevoToolStripMenuItem"
        Me.NuevoToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.NuevoToolStripMenuItem.Text = "&Nuevo"
        '
        'AbrirToolStripMenuItem
        '
        Me.AbrirToolStripMenuItem.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.AbrirToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.AbrirToolStripMenuItem.Name = "AbrirToolStripMenuItem"
        Me.AbrirToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.AbrirToolStripMenuItem.Text = "&Abrir"
        '
        'GuardarToolStripMenuItem
        '
        Me.GuardarToolStripMenuItem.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.GuardarToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.GuardarToolStripMenuItem.Name = "GuardarToolStripMenuItem"
        Me.GuardarToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.GuardarToolStripMenuItem.Text = "&Guardar"
        '
        'GuardarComoToolStripMenuItem
        '
        Me.GuardarComoToolStripMenuItem.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.GuardarComoToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.GuardarComoToolStripMenuItem.Name = "GuardarComoToolStripMenuItem"
        Me.GuardarComoToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.GuardarComoToolStripMenuItem.Text = "Guardar &Como..."
        '
        'ConsolaToolStripMenuItem
        '
        Me.ConsolaToolStripMenuItem.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ConsolaToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.ConsolaToolStripMenuItem.Name = "ConsolaToolStripMenuItem"
        Me.ConsolaToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.ConsolaToolStripMenuItem.Text = "Consola"
        '
        'EditarToolStripMenuItem
        '
        Me.EditarToolStripMenuItem.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.EditarToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CopiarToolStripMenuItem, Me.CortarToolStripMenuItem, Me.PegarToolStripMenuItem, Me.BuscarToolStripMenuItem})
        Me.EditarToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.EditarToolStripMenuItem.Name = "EditarToolStripMenuItem"
        Me.EditarToolStripMenuItem.Size = New System.Drawing.Size(49, 20)
        Me.EditarToolStripMenuItem.Text = "Editar"
        '
        'CopiarToolStripMenuItem
        '
        Me.CopiarToolStripMenuItem.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.CopiarToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.CopiarToolStripMenuItem.Name = "CopiarToolStripMenuItem"
        Me.CopiarToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.CopiarToolStripMenuItem.Text = "Copiar"
        '
        'CortarToolStripMenuItem
        '
        Me.CortarToolStripMenuItem.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.CortarToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.CortarToolStripMenuItem.Name = "CortarToolStripMenuItem"
        Me.CortarToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.CortarToolStripMenuItem.Text = "Cortar"
        '
        'PegarToolStripMenuItem
        '
        Me.PegarToolStripMenuItem.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.PegarToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.PegarToolStripMenuItem.Name = "PegarToolStripMenuItem"
        Me.PegarToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.PegarToolStripMenuItem.Text = "Pegar"
        '
        'BuscarToolStripMenuItem
        '
        Me.BuscarToolStripMenuItem.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.BuscarToolStripMenuItem.Enabled = False
        Me.BuscarToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.BuscarToolStripMenuItem.Name = "BuscarToolStripMenuItem"
        Me.BuscarToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.BuscarToolStripMenuItem.Text = "Buscar"
        '
        'AyudaToolStripMenuItem
        '
        Me.AyudaToolStripMenuItem.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.AyudaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AyudaToolStripMenuItem1, Me.AcercaDeToolStripMenuItem})
        Me.AyudaToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.AyudaToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.AyudaToolStripMenuItem.Name = "AyudaToolStripMenuItem"
        Me.AyudaToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.AyudaToolStripMenuItem.Text = "Ayuda"
        '
        'AyudaToolStripMenuItem1
        '
        Me.AyudaToolStripMenuItem1.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.AyudaToolStripMenuItem1.ForeColor = System.Drawing.Color.White
        Me.AyudaToolStripMenuItem1.Name = "AyudaToolStripMenuItem1"
        Me.AyudaToolStripMenuItem1.Size = New System.Drawing.Size(152, 22)
        Me.AyudaToolStripMenuItem1.Text = "Ayuda"
        '
        'AcercaDeToolStripMenuItem
        '
        Me.AcercaDeToolStripMenuItem.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.AcercaDeToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.AcercaDeToolStripMenuItem.Name = "AcercaDeToolStripMenuItem"
        Me.AcercaDeToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AcercaDeToolStripMenuItem.Text = "Acerca de..."
        '
        'CompilarToolStripMenuItem
        '
        Me.CompilarToolStripMenuItem.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.CompilarToolStripMenuItem.Enabled = False
        Me.CompilarToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.CompilarToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.CompilarToolStripMenuItem.Name = "CompilarToolStripMenuItem"
        Me.CompilarToolStripMenuItem.Size = New System.Drawing.Size(68, 20)
        Me.CompilarToolStripMenuItem.Text = "Compilar"
        '
        'tabMain
        '
        Me.tabMain.Controls.Add(Me.Page1)
        Me.tabMain.Controls.Add(Me.PageAdd)
        Me.tabMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabMain.ImageList = Me.imgList
        Me.tabMain.ItemSize = New System.Drawing.Size(30, 19)
        Me.tabMain.Location = New System.Drawing.Point(0, 24)
        Me.tabMain.Multiline = True
        Me.tabMain.Name = "tabMain"
        Me.tabMain.Padding = New System.Drawing.Point(5, 4)
        Me.tabMain.SelectedIndex = 0
        Me.tabMain.Size = New System.Drawing.Size(637, 302)
        Me.tabMain.TabIndex = 6
        '
        'Page1
        '
        Me.Page1.Controls.Add(Me.rtxtMain)
        Me.Page1.ImageIndex = 0
        Me.Page1.Location = New System.Drawing.Point(4, 23)
        Me.Page1.Name = "Page1"
        Me.Page1.Padding = New System.Windows.Forms.Padding(3)
        Me.Page1.Size = New System.Drawing.Size(629, 275)
        Me.Page1.TabIndex = 0
        Me.Page1.Text = "NewPage"
        Me.Page1.UseVisualStyleBackColor = True
        '
        'rtxtMain
        '
        Me.rtxtMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtxtMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtxtMain.Location = New System.Drawing.Point(3, 3)
        Me.rtxtMain.Name = "rtxtMain"
        Me.rtxtMain.Size = New System.Drawing.Size(623, 269)
        Me.rtxtMain.TabIndex = 0
        Me.rtxtMain.Text = ""
        '
        'PageAdd
        '
        Me.PageAdd.Controls.Add(Me.btnAct)
        Me.PageAdd.Controls.Add(Me.btnAbrir)
        Me.PageAdd.ImageIndex = 2
        Me.PageAdd.Location = New System.Drawing.Point(4, 23)
        Me.PageAdd.Name = "PageAdd"
        Me.PageAdd.Padding = New System.Windows.Forms.Padding(3)
        Me.PageAdd.Size = New System.Drawing.Size(471, 224)
        Me.PageAdd.TabIndex = 2
        Me.PageAdd.Text = "New"
        Me.PageAdd.UseVisualStyleBackColor = True
        '
        'btnAct
        '
        Me.btnAct.Location = New System.Drawing.Point(112, 118)
        Me.btnAct.Name = "btnAct"
        Me.btnAct.Size = New System.Drawing.Size(237, 37)
        Me.btnAct.TabIndex = 0
        Me.btnAct.Text = "Buscar Actualizaciones...."
        Me.btnAct.UseVisualStyleBackColor = True
        '
        'btnAbrir
        '
        Me.btnAbrir.Location = New System.Drawing.Point(112, 49)
        Me.btnAbrir.Name = "btnAbrir"
        Me.btnAbrir.Size = New System.Drawing.Size(237, 37)
        Me.btnAbrir.TabIndex = 0
        Me.btnAbrir.Text = "Abrir nuevo archivo de texto"
        Me.btnAbrir.UseVisualStyleBackColor = True
        '
        'imgList
        '
        Me.imgList.ImageStream = CType(resources.GetObject("imgList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgList.TransparentColor = System.Drawing.Color.White
        Me.imgList.Images.SetKeyName(0, "close.png")
        Me.imgList.Images.SetKeyName(1, "closeHover.png")
        Me.imgList.Images.SetKeyName(2, "close.png")
        Me.imgList.Images.SetKeyName(3, "closeHover.png")
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblStat, Me.ToolStripProgressBar1, Me.ToolStripStatusLabel3, Me.ToolStripStatusLabel2, Me.txtCuentaPalabras, Me.ToolStripStatusLabel1, Me.lbl, Me.txtLineaActual, Me.ToolStripStatusLabel4, Me.txtCuentaLinea, Me.ToolStripStatusLabel5, Me.lbl1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 326)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(637, 22)
        Me.StatusStrip1.TabIndex = 7
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblStat
        '
        Me.lblStat.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter
        Me.lblStat.ForeColor = System.Drawing.Color.White
        Me.lblStat.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.lblStat.Name = "lblStat"
        Me.lblStat.Size = New System.Drawing.Size(0, 17)
        '
        'ToolStripProgressBar1
        '
        Me.ToolStripProgressBar1.AutoSize = False
        Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
        Me.ToolStripProgressBar1.Size = New System.Drawing.Size(53, 16)
        Me.ToolStripProgressBar1.Step = 1
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.AutoSize = False
        Me.ToolStripStatusLabel3.BackColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel3.Enabled = False
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(1, 17)
        Me.ToolStripStatusLabel3.Text = " "
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripStatusLabel2.ForeColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(47, 17)
        Me.ToolStripStatusLabel2.Text = "--------"
        '
        'txtCuentaPalabras
        '
        Me.txtCuentaPalabras.ForeColor = System.Drawing.Color.White
        Me.txtCuentaPalabras.Name = "txtCuentaPalabras"
        Me.txtCuentaPalabras.Size = New System.Drawing.Size(13, 17)
        Me.txtCuentaPalabras.Text = "0"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.AutoSize = False
        Me.ToolStripStatusLabel1.BackColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel1.Enabled = False
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(1, 17)
        Me.ToolStripStatusLabel1.Text = " "
        '
        'lbl
        '
        Me.lbl.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lbl.ForeColor = System.Drawing.Color.White
        Me.lbl.Name = "lbl"
        Me.lbl.Size = New System.Drawing.Size(39, 17)
        Me.lbl.Text = "Linea:"
        '
        'txtLineaActual
        '
        Me.txtLineaActual.ForeColor = System.Drawing.Color.White
        Me.txtLineaActual.Name = "txtLineaActual"
        Me.txtLineaActual.Size = New System.Drawing.Size(13, 17)
        Me.txtLineaActual.Text = "0"
        '
        'ToolStripStatusLabel4
        '
        Me.ToolStripStatusLabel4.ForeColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel4.Name = "ToolStripStatusLabel4"
        Me.ToolStripStatusLabel4.Size = New System.Drawing.Size(20, 17)
        Me.ToolStripStatusLabel4.Text = "de"
        '
        'txtCuentaLinea
        '
        Me.txtCuentaLinea.ForeColor = System.Drawing.Color.White
        Me.txtCuentaLinea.Name = "txtCuentaLinea"
        Me.txtCuentaLinea.Size = New System.Drawing.Size(13, 17)
        Me.txtCuentaLinea.Text = "0"
        '
        'ToolStripStatusLabel5
        '
        Me.ToolStripStatusLabel5.AutoSize = False
        Me.ToolStripStatusLabel5.BackColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel5.Enabled = False
        Me.ToolStripStatusLabel5.Name = "ToolStripStatusLabel5"
        Me.ToolStripStatusLabel5.Size = New System.Drawing.Size(1, 17)
        Me.ToolStripStatusLabel5.Text = " "
        '
        'lbl1
        '
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(0, 17)
        '
        'oArchivo
        '
        Me.oArchivo.DefaultExt = "txt"
        Me.oArchivo.Filter = "Archivos de Texto|*.txt"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(637, 348)
        Me.Controls.Add(Me.tabMain)
        Me.Controls.Add(Me.mainStrip)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "Form1"
        Me.Text = "IDE ahi pa' donde?"
        Me.mainStrip.ResumeLayout(False)
        Me.mainStrip.PerformLayout()
        Me.tabMain.ResumeLayout(False)
        Me.Page1.ResumeLayout(False)
        Me.PageAdd.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mainStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents ArchivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NuevoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AbrirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GuardarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GuardarComoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CopiarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CortarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PegarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BuscarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents CompilarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents tabMain As System.Windows.Forms.TabControl
    Private WithEvents Page1 As System.Windows.Forms.TabPage
    Friend WithEvents rtxtMain As System.Windows.Forms.RichTextBox
    Friend WithEvents PageAdd As System.Windows.Forms.TabPage
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lblStat As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripProgressBar1 As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtCuentaPalabras As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lbl As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtLineaActual As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel4 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtCuentaLinea As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel5 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lbl1 As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents imgList As System.Windows.Forms.ImageList
    Friend WithEvents oArchivo As System.Windows.Forms.OpenFileDialog
    Friend WithEvents sArchivo As System.Windows.Forms.SaveFileDialog
    Private WithEvents AyudaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AyudaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AcercaDeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnAct As System.Windows.Forms.Button
    Friend WithEvents btnAbrir As System.Windows.Forms.Button
    Friend WithEvents ConsolaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
